use crate::models::FeedlyError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed to Parse URL")]
    Url(#[from] url::ParseError),
    #[error("Failed to (de)serialize Json")]
    Json {
        source: serde_json::error::Error,
        json: String,
    },
    #[error("Failed to manually deserialize response")]
    ManualJson,
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Feedly specific error")]
    Feedly(FeedlyError),
    #[error("Malformed input arguments")]
    Input,
    #[error("No valid access token available")]
    Token,
    #[error("Request failed with message access denied")]
    AccessDenied,
    #[error("Internal error")]
    InternalMutabilty,
    #[error("Access token expired")]
    TokenExpired,
    #[error("Unknown Error")]
    Unknown,
}

impl ApiError {
    pub fn parse_feedly_error(error: FeedlyError) -> Self {
        if error.error_message.starts_with("token expired") {
            Self::TokenExpired
        } else {
            Self::Feedly(error)
        }
    }
}
