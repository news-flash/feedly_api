use feedly_api::FeedlyApi;
use std::io::Write;
use std::{env, process};
use url::Url;

#[tokio::main]
async fn main() {
    // A client ID and secret issued by Feedly are required. Provide them here via environment
    // variables.
    let client_id = match env::var("FEEDLY_CLIENT_ID") {
        Ok(client_id) => client_id,
        Err(_) => {
            eprintln!("FEEDLY_CLIENT_ID not defined");
            process::exit(1);
        }
    };
    let client_secret = match env::var("FEEDLY_CLIENT_SECRET") {
        Ok(client_secret) => client_secret,
        Err(_) => {
            eprintln!("FEEDLY_CLIENT_SECRET not defined");
            process::exit(1);
        }
    };

    // When opened in a browser, the login URL will redirect to a URL which is needed for the next
    // step.
    let login_url = FeedlyApi::login_url(&client_id, &client_secret).unwrap();
    println!("Login URL: {}", login_url);
    print!("Redirect URL: ");
    std::io::stdout().flush().unwrap();
    let mut redirect_url = String::new();
    std::io::stdin().read_line(&mut redirect_url).unwrap();

    // Parse the redirected URL after successful login to obtain the auth code.
    let auth_code = FeedlyApi::parse_redirected_url(&Url::parse(&redirect_url).unwrap()).unwrap();

    // With the auth code from the previous step, the access_token and friends can finally be
    // requested.
    let client = reqwest::Client::new();
    let token_response =
        FeedlyApi::request_auth_token(&client_id, &client_secret, auth_code, &client)
            .await
            .unwrap();
    let access_token_expires =
        FeedlyApi::parse_expiration_date(&token_response.expires_in.to_string()).unwrap();

    // Finally, create a FeedlyApi instance.
    let feedly_api = FeedlyApi::new(
        client_id,
        client_secret,
        token_response.access_token,
        token_response.refresh_token,
        access_token_expires,
    )
    .unwrap();

    // Use the FeedlyApi instance to send requests to Feedly.
    let profile = feedly_api.get_profile(&client).await.unwrap();
    println!("Successfully logged in as {}", profile.id);
}
