use crate::ApiError;
use serde_derive::{Deserialize, Serialize};
use serde_json::Value;

/// Tags allow users to collect individual entries.
/// The format for a tag id is `user/:userId/tag/:label`.
/// The laber of a tag is initally set to the last part of the id.
/// The label cannot contain any of the following characters `"`, `<`, `>`, `?`, `&`, `/`, `\`, `^`
#[derive(Debug, Serialize, Deserialize)]
pub struct Tag {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
}

impl Tag {
    pub fn decompose(self) -> (String, Option<String>, Option<String>) {
        (self.id, self.label, self.description)
    }

    pub fn manual_deserialize(data: &Value) -> Result<Tag, ApiError> {
        let id = data["id"].as_str().ok_or(ApiError::ManualJson)?.into();

        let label = data["label"].as_str().map(|t| t.into());

        let description = data["description"].as_str().map(|t| t.into());

        Ok(Tag {
            id,
            label,
            description,
        })
    }

    pub fn manual_deserialize_vec(data: &Value) -> Result<Vec<Tag>, ApiError> {
        let mut tags = Vec::new();

        let vector = match data.as_array() {
            Some(vector) => vector,
            None => {
                log::warn!("Deserializing tag vector: empty");
                return Ok(tags);
            }
        };

        for tag_value in vector {
            tags.push(Self::manual_deserialize(tag_value)?);
        }

        Ok(tags)
    }
}
