use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct SearchResult {
    /// An auto-completion guess of the keyword the user is trying to search for.
    pub hint: Option<String>,

    /// A list of other keywords the user might be interested in searching.
    pub related: Option<Vec<String>>,

    /// A list of feeds matching the search query.
    /// Feeds which are sponsored should be listed above feeds which are featured and above other search results.
    pub results: Vec<SearchResultItem>,
}

impl SearchResult {
    pub fn decompose(self) -> (Option<String>, Option<Vec<String>>, Vec<SearchResultItem>) {
        (self.hint, self.related, self.results)
    }
}

#[derive(Debug, Deserialize)]
pub struct SearchResultItem {
    /// The unique, immutable id of this feed.
    #[serde(rename = "feedId")]
    pub feed_id: String,

    /// Number of feedly cloud subscribers who have this feed in their subscription list.
    pub subscribers: u32,

    /// The feed name.
    pub title: Option<String>,

    /// The feed description.
    pub description: Option<String>,

    /// The website associated with this feed.
    pub website: Option<String>,

    /// The timestamp, in ms, of the last article received for this feed.
    /// This value is useful to find "dormant" feeds (feeds that haven't updated in over 3 months).
    #[serde(rename = "lastUpdated")]
    pub last_updated: Option<i64>,

    /// The average number of articles published weekly.
    /// This number is updated every few days.
    pub velocity: Option<f32>,

    /// This field is a combination of the language reported by the RSS feed, and the language automatically detected from the feed's content.
    /// It might not be accurate, as many feeds misreport it.
    pub language: Option<String>,

    /// If true, this feed is curated.
    pub curated: Option<bool>,

    /// If true, this feed is featured (recommended) for the topic or search query.
    pub featured: Option<bool>,

    /// ???
    pub partial: Option<bool>,

    /// Url to a small square icon.
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,

    /// Url to a larger square icon.
    #[serde(rename = "visualUrl")]
    pub visual_url: Option<String>,

    /// Url to a large rectangular background image.
    #[serde(rename = "coverUrl")]
    pub cover_url: Option<String>,

    /// Url to a small suqare icon with transparency.
    pub logo: Option<String>,

    /// The auto-detected type of entries this feed publishes.
    /// Values include
    /// * `article` (most common)
    /// * `longform` (for longer articles)
    /// * `videos` (for YouTube, Vimeo and other video-centric feeds)
    /// * `audio` (for podcast feeds etc)
    #[serde(rename = "contentType")]
    pub content_type: Option<String>,

    /// Hex color for the background cover
    #[serde(rename = "coverColor")]
    pub cover_color: Option<String>,

    /// ???
    #[serde(rename = "deliciousTags")]
    pub delicious_tags: Option<Vec<String>>,
}
