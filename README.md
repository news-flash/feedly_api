# feedly_api

[![Crates.io Status](https://img.shields.io/crates/v/feed-rs.svg)](https://crates.io/crates/feedly_api)

Rust implementation of the [feedly API](https://developer.feedly.com/).

## Usage

Add the dependency to your `Cargo.toml`.

```toml
[dependencies]
feedly_api = "0.6"
```

## Getting up and running

See the [login example](examples/login.rs) for how to log in and obtain a
`FeedlyApi` instance.

Run the example using `cargo`:

```
cargo run --example login
```
