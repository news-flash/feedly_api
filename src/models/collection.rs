use super::Subscription;
use crate::ApiError;
use serde_derive::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Collection {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub feeds: Option<Vec<Subscription>>,
}

impl Collection {
    pub fn decompose(
        self,
    ) -> (
        String,
        Option<String>,
        Option<String>,
        Option<Vec<Subscription>>,
    ) {
        (self.id, self.label, self.description, self.feeds)
    }

    pub fn manual_deserialize(data: &Value) -> Result<Collection, ApiError> {
        let id = data["id"].as_str().ok_or(ApiError::ManualJson)?.into();

        let label = data["label"].as_str().map(|t| t.into());

        let description = data["description"].as_str().map(|t| t.into());

        let feeds: Option<Vec<Subscription>> = match serde_json::from_value(data["feeds"].clone()) {
            Ok(result) => Some(result),
            Err(_) => None,
        };

        Ok(Collection {
            id,
            label,
            description,
            feeds,
        })
    }

    pub fn manual_deserialize_vec(data: &Value) -> Result<Vec<Collection>, ApiError> {
        let mut collections = Vec::new();

        let vector = match data.as_array() {
            Some(vector) => vector,
            None => return Ok(collections),
        };

        for collection_value in vector {
            collections.push(Self::manual_deserialize(collection_value)?);
        }

        Ok(collections)
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct CollectionInput {
    pub label: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub feeds: Option<Vec<CollectionFeedInput>>,
}

#[derive(Clone, Debug, Serialize)]
pub struct CollectionFeedInput {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
}
