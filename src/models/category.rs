use crate::ApiError;
use serde_derive::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Category {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub description: Option<String>,
}

impl Category {
    pub fn decompose(self) -> (String, Option<String>, Option<String>) {
        (self.id, self.label, self.description)
    }

    pub fn manual_deserialize(data: &Value) -> Result<Category, ApiError> {
        let id = data["id"].as_str().ok_or(ApiError::ManualJson)?.into();

        let label = data["label"].as_str().map(|t| t.into());

        let description = data["description"].as_str().map(|t| t.into());

        Ok(Category {
            id,
            label,
            description,
        })
    }

    pub fn manual_deserialize_vec(data: &Value) -> Result<Vec<Category>, ApiError> {
        let mut categories = Vec::new();

        let vector = match data.as_array() {
            Some(vector) => vector,
            None => return Ok(categories),
        };

        for category_value in vector {
            categories.push(Self::manual_deserialize(category_value)?);
        }

        Ok(categories)
    }
}
